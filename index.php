<?php

require_once 'classes/Queue.php';
require_once 'classes/Stack.php';

$queue = new Queue();
$queue->addToQueue('a');
$queue->addToQueue('b');
$queue->addToQueue('c');
$queue->addToQueue('d');
$queue->addToQueue('e');

echo 'Длина очереди: '. $queue->getQueueLength ();
echo "</br>";
 echo'Очередь: ';
$queue->showQueue();
echo "</br>";
$queue->removeFromQueue();
echo'Очередь: ';
$queue->showQueue();
$queue->clearQueue();
echo "</br>";
echo'Очередь: ';
$queue->showQueue();

echo "</br></br></br>";

$stack = new Stack();
$stack->push('a');
$stack->push('b');
$stack->push('c');
$stack->push('d');
$stack->push('e');
echo "Стек: ";
$stack->show();
echo "</br>";
echo'Верхний элемент стека - '. $stack->getTop ();
echo "</br>";
echo'Длина стека: '. $stack->getLength ();
echo "</br>";
$stack->pop();
 echo "Стек: ";
$stack->show();
echo "</br>";
echo'Длина стека: '. $stack->getLength ();
echo "</br>";
$stack->clearStack();
echo "Очиста стека. Длина стека: " . $stack->getLength ();