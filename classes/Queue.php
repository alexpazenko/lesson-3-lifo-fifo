<?php

class Queue {

    public $queue = array();

    public function getQueueLength() {
        if (!empty($this->queue)){
        return count($this->queue);
        } else {
            return false;
        }
    }

    public function addToQueue($element) {
         array_push($this->queue, $element);
    }

    public function removeFromQueue() {
        if (!empty($this->queue)) {
            return array_shift($this->queue);
        }
        return false;
    }

    public function showQueue() {
        if (!empty($this->queue)) {
            for ($i = 0; $i < $this->getQueueLength(); $i++) {
                echo $this->queue[$i];
            }
        } else {
            echo "пустая";
        }
    }

    public function clearQueue() {
        $this->queue = array();
    }
}