<?php

class Stack
{

    public $stack = array();

    public function push($element) {
       array_push($this->stack, $element);
    }

    public function pop() {
        if (!empty($this->stack)) {
            return array_pop($this->stack);
        }
    }

    public function getTop() {
        $top = count($this->stack) - 1;
        return $this->stack[$top];
    }

    public function clearStack() {
         $this->stack = array();
    }

    public function show() {
        if (!empty($this->stack)) {
            for ($i = 0; $i < count($this->stack); $i++) {
                echo $this->stack[$i] . PHP_EOL;
            }
        } else {
            echo "Пустой!";
        }
    }

    public function getLength()
    {
        return count($this->stack);
    }
}